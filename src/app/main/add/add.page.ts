import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { MainService } from "../main.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formAddMatch: FormGroup;

  constructor(private serviceMain: MainService, private router: Router) { }

  ngOnInit() {

    this.formAddMatch = new FormGroup({
      pteamOne: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pteamTwo: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
    });
  }

  addMatch() {
    //add match to array

    if (!this.formAddMatch.valid) {
      return;
    }

    this.serviceMain.addMatch(
      this.formAddMatch.value.pcodeMatch,
      this.formAddMatch.value.pteamOne,
      this.formAddMatch.value.pscoreOne,
      this.formAddMatch.value.pteamTwo,
      this.formAddMatch.value.pscoreTwo,
    );

    this.formAddMatch.reset(); //clean form

    this.router.navigate(["/main"]);
  }

}
