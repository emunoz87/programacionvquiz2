import { Component, OnInit } from '@angular/core';
import { MainService } from "./main.service";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { NavController, ToastController } from "@ionic/angular";
import { match } from "./main.model";

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  match: match[];

  constructor(
    private mainServices: MainService,
    private router: Router,
    public navCtrl: NavController,
    public toastCtrl: ToastController
  ) { }

  ngOnInit() {

    //load initial data
    this.match = this.mainServices.getAll();
    console.log (this.match);
  }

  ionViewWillEnter() {
    this.match = this.mainServices.getAll();
  }

  view(code: number) {
    this.router.navigate(["/main/view/" + code]);
  }
}


