import { Injectable } from '@angular/core';
import { match, event } from "./main.model";

@Injectable({
  providedIn: 'root'
})
export class MainService {

  //events array
  private event: event[] = [
    {
      codeMatch: 1,
      eventName: "Falta",
      team: "Heredia",
      minute: 50,
    }
  ];

  //matches array
  private match: match[] = [{

      event: this.event,
      codeMatch: 1,
      teamOne: "LDA",
      scoreOne: 0,
      teamTwo: "Heredia",
      scoreTwo: 0,
      button: 0,

  }];

  constructor() { }

  getAll() {
    return [...this.match];
  }

  getMatch(matchId: number) {
    return {
      ...this.match.find((match) => {
        return match.codeMatch === matchId;
      }),
    };
  }

  setButton(code: number) {
    let index = this.match.map((x) => x.codeMatch).indexOf(code);
    this.match[index].button = 1;
  }

  addMatch(
    pcodeMatch: number,
    pteamOne: string,
    pscoreOne: number,
    pteamTwo: string,
    pscoreTwo: number,
  ) {
    const match: match = {
      event: this.event,
      codeMatch: Math.round(Math.random() * (100 - 3) + 3),
      teamOne: pteamOne,
      scoreOne: 0,
      teamTwo: pteamTwo,
      scoreTwo: 0,
      button: 0,
    };
    this.match.push(match);
  }

  addEvent(
     pcodeMatch: number,
     pevent: string,
     pteam: string,
     pminute: number
   ) {
     const event: event = {
       codeMatch: pcodeMatch,
       eventName: pevent,
       team: pteam,
       minute: pminute,
     };

     if (event.eventName == "Gol") {
       let index = this.match.map((x) => x.codeMatch).indexOf(pcodeMatch); //get the index of the element

       if (this.match[index].teamOne == event.team) {
         this.match[index].scoreOne++;
       } else {
         this.match[index].scoreTwo++;
       }
     }

     this.event.push(event);
   }

}





