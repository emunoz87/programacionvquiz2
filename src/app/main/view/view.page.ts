import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { MainService } from "../main.service";
import { match } from "../main.model";

@Component({
  selector: 'app-view',
  templateUrl: './view.page.html',
  styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {
  mtch: match;
  public disabled = false;
  id: number;

  constructor(
    private activeRouter: ActivatedRoute,
    private mainServices: MainService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.activeRouter.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("matchId")) {
        return;
      }
      const matchId = parseInt(paramMap.get("matchId"));
      this.mtch = this.mainServices.getMatch(matchId);
      this.id = matchId;

      if (this.mtch.button == 0) {
        this.disabled = false;
      } else {
        this.disabled = true;
      }
    });
  }

  view(code: number) {
    this.router.navigate(["/main/event/" + code]);
  }

  public action() {
    this.mainServices.setButton(this.id);
    this.disabled = true;
  }

}
