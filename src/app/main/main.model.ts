export interface match{
    
    codeMatch:number;
    teamOne: string;
    scoreOne: number
    teamTwo: string;
    scoreTwo:number;
    button:number;
    event: event[];
}

export interface event{
    codeMatch:number;
    eventName: string;  
    team: string;
    minute: number;
}