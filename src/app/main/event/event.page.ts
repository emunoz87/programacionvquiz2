import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MainService } from "../main.service";
import { match } from "../main.model";

@Component({
  selector: 'app-event',
  templateUrl: './event.page.html',
  styleUrls: ['./event.page.scss'],
})
export class EventPage implements OnInit {

  formAddEvent: FormGroup;
  idMtch: number;
  mtch: match;

  constructor(
    private activeRouter: ActivatedRoute,
    private mainServices: MainService,
    private router: Router
  ) { }

  ngOnInit() {

    this.formAddEvent = new FormGroup({
      pevent: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pteam: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      pminute: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
    });

    //obtain the parameter to search the product and then the index

    this.activeRouter.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("matchId")) {
        return;
      }
      const mtchId = parseInt(paramMap.get("matchId"));
      this.mtch = this.mainServices.getMatch(mtchId);
      this.idMtch = mtchId;
    });
  }
  
  addEvent() {
    //add event to array

    if (!this.formAddEvent.valid) {
      return;
    }

    this.mainServices.addEvent(
      this.formAddEvent.value.codeMatch = this.idMtch,
      this.formAddEvent.value.pevento,
      this.formAddEvent.value.pequipo,
      this.formAddEvent.value.pminute
    );

    this.formAddEvent.reset(); //clean form

    this.router.navigate(["/main"]);
  }
}